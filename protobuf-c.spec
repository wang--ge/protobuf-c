Name:           protobuf-c
Version:        1.4.0
Release:        2
Summary:        This is protobuf-c, a C implementation of the Google Protocol Buffers data serialization format
License:        BSD-2-Clause
URL:            https://github.com/protobuf-c/protobuf-c
Source0:        %{url}/archive/refs/tags/v%{version}.tar.gz 
BuildRequires:  autoconf automake libtool gcc-c++ pkgconfig(protobuf)
Provides:       %{name}-compiler = %{version}-%{release}
Obsoletes:      %{name}-compiler < %{version}-%{release}

%description
This is protobuf-c, a C implementation of the Google Protocol Buffers data serialization format.


%package devel
Summary:        Development files for protobuf-c library
Requires:       %{name} = %{version}-%{release} %{name}-compiler = %{version}-%{release}

%description devel
Protobuf-c-devel contains development files for protobuf-c.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ifv
%configure --disable-static
%make_build

%check
make check

%install
%make_install
%delete_la

%ldconfig_scriptlets

%files
%doc TODO LICENSE ChangeLog
%{_libdir}/libprotobuf-c.so.*
%{_bindir}/{protoc-c,protoc-gen-c}

%files devel
%dir %{_includedir}/google
%{_includedir}/{google/protobuf-c/,protobuf-c/}
%{_libdir}/{libprotobuf-c.so,pkgconfig/libprotobuf-c.pc}

%changelog
* Tue May 10 2022 Ge Wang <wangge20@h-partner.com> - 1.4.0-2
- License compliance rectification

* Thu Dec 02 2021 wujing <wujing50@huawei.com> - 1.4.0-1
Type:upgrade
ID:NA
SUG:NA
DESC: upgrade to 1.4.0-1

* Fri Feb 14 2020 Senlin Xia <xiasenlin1@huawei.com> - 1.3.2-2
- Package init
